import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'data.dart';
import 'dart:math';

bool isIOS = true;

void main() => runApp(MaterialApp(
      home: AdaptivePageScaffold(
        child: ImageCarousel(),
        title: 'Welcome Home',
      ),
      debugShowCheckedModeBanner: false,
    ));

class ImageCarousel extends StatefulWidget {
  ImageCarousel();
  @override
  _ImageCarouselState createState() => new _ImageCarouselState();
}

var cardAspectRatio = 12.0 / 16.0;
var widgetAspectRatio = cardAspectRatio * 1.2;
var currentPage = images.length - 1.0;

class _ImageCarouselState extends State<ImageCarousel> {
  _ImageCarouselState();
  @override
  Widget build(BuildContext context) {
    PageController controller = PageController(initialPage: images.length - 1);
    controller.addListener(() {
      if (this.mounted) {
        setState(() {
          currentPage = controller.page;
        });
      }
    });

    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
            colors: [
              CupertinoColors.white,
              CupertinoColors.activeBlue,
              CupertinoColors.extraLightBackgroundGray
            ],
            begin: Alignment.bottomCenter,
            end: Alignment.topCenter,
            tileMode: TileMode.clamp),
      ),
      child: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: _getCardScrollable(controller),
          ),
        ],
      ),
    );
  }
}

class CardScrollWidget extends StatelessWidget {
  var currentPage;
  var padding = 20.0;
  var verticalInset = 20.0;

  CardScrollWidget(this.currentPage);

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: widgetAspectRatio,
      child: LayoutBuilder(builder: (context, contraints) {
        var width = contraints.maxWidth;
        var height = contraints.maxHeight;

        var safeWidth = width - 2 * padding;
        var safeHeight = height - 2 * padding;

        var heightOfPrimaryCard = safeHeight;
        var widthOfPrimaryCard = heightOfPrimaryCard * cardAspectRatio;

        var primaryCardLeft = safeWidth - widthOfPrimaryCard;
        var horizontalInset = primaryCardLeft / 2;

        List<Widget> cardList = new List();

        for (var i = 0; i < images.length; i++) {
          var delta = i - currentPage;
          bool isOnRight = delta > 0;

          var start = padding +
              max(
                  primaryCardLeft -
                      horizontalInset * -delta * (isOnRight ? 15 : 1),
                  0.0);

          var cardItem = Positioned.directional(
            top: padding + verticalInset * max(-delta, 0.0),
            bottom: padding + verticalInset * max(-delta, 0.0),
            start: start,
            textDirection: TextDirection.rtl,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(16.0),
              child: Container(
                decoration:
                    BoxDecoration(color: CupertinoColors.white, boxShadow: [
                  BoxShadow(
                      color: CupertinoColors.black,
                      offset: Offset(3.0, 6.0),
                      blurRadius: 10.0)
                ]),
                child: AspectRatio(
                  aspectRatio: cardAspectRatio,
                  child: Stack(
                    fit: StackFit.expand,
                    children: <Widget>[
                      Image.asset(images[i], fit: BoxFit.cover),
                    ],
                  ),
                ),
              ),
            ),
          );
          cardList.add(cardItem);
        }
        return Stack(
          children: cardList,
        );
      }),
    );
  }
}

class AdaptivePageScaffold extends StatelessWidget {
  const AdaptivePageScaffold({
    @required this.title,
    @required this.child,
  })  : assert(title != null),
        assert(child != null);

  final String title;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    if (isIOS) {
      return HomeCupertinoPage();
    } else {
      return Scaffold(
        appBar: AppBar(
          title: Text(title),
          backgroundColor: CupertinoColors.activeGreen,
        ),
        drawer:
            ModalRoute.of(context).isFirst ? MainDrawer(child: child) : null,
      );
    }
  }
}

class MainDrawer extends StatelessWidget {
  const MainDrawer({@required this.child}) : assert(child != null);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          DrawerHeader(
            decoration: BoxDecoration(
              color: CupertinoColors.activeGreen,
            ),
            child: Align(
              alignment: Alignment.bottomLeft,
              child: Text(
                'Ñe\'enga Teller',
                style: TextStyle(
                  fontFamily: 'SF-Pro-Text-Regular',
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                  fontStyle: FontStyle.italic,
                ),
              ),
            ),
          ),
          ListTile(
            onTap: () {
              Navigator.of(context)
                ..pop(context)
                ..push(
                  MaterialPageRoute(
                    builder: (context) => PaginaImagenes(
                      child: child,
                    ),
                  ),
                );
            },
            leading: Icon(Icons.book),
            title: Text('Ñe\'engas'),
          ),
          ListTile(
            onTap: () {
              Navigator.of(context)
                ..pop(context)
                ..push(MaterialPageRoute(
                  builder: (context) => Center(
                    child: child,
                  ),
                ));
            },
            leading: Icon(Icons.info_outline),
            title: Text('Add from List'),
          ),
        ],
      ),
    );
  }
}

class PaginaImagenes extends StatelessWidget {
  const PaginaImagenes({@required this.child}) : assert(child != null);

  final Widget child;
  @override
  Widget build(BuildContext context) {
    return Center(
      child: child,
    );
  }
}

Widget _getCardScrollable(PageController controller) {
  return Stack(
    children: <Widget>[
      CardScrollWidget(currentPage),
      Positioned.fill(
        child: PageView.builder(
          itemCount: images.length,
          controller: controller,
          reverse: true,
          itemBuilder: (context, index) {
            return Container();
          },
        ),
      ),
    ],
  );
}

class HomeCupertinoPage extends StatelessWidget {
  const HomeCupertinoPage();

  @override
  Widget build(BuildContext context) {
    return CupertinoTabScaffold(
      tabBar: CupertinoTabBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.home),
            title: Text('Roga'),
          ),
          BottomNavigationBarItem(
              icon: Icon(
                CupertinoIcons.info,
              ),
              title: Text('Ore'),
              backgroundColor: CupertinoColors.activeGreen),
        ],
      ),
      tabBuilder: (BuildContext context, int index) {
        assert(index >= 0 && index <= 1);
        switch (index) {
          case 0:
            return CupertinoTabView(
              builder: (BuildContext context) => _buildHomePage(context),
            );
            break;
          case 1:
            return CupertinoTabView(
              builder: (BuildContext context) => buildPage('Ore'),
            );
            break;
          default:
            break;
        }
        return null;
      },
    );
  }
}

Widget buildPage(String title) {
  return CupertinoPageScaffold(
    child: Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          ZoomClipAssetImage(
            height: 112,
            width: 112,
            zoom: 2.4,
            imageAsset: 'assets/imk.jpg',
          ),
          Text(''),
          Center(
            child: Text(
              'Esta aplicación fue desarrollada en conjunto \n' +
                  'con los miembros de la organizacion de la \ncarrera de Ingenieria Marketing' +
                  ' - ETyC 2019.',
              style: TextStyle(
                  fontSize: 12.0,
                  color: CupertinoColors.activeBlue,
                  decorationColor: CupertinoColors.white,
                  fontStyle: FontStyle.italic,
                  fontFamily: 'SF-Pro-Text-Regular'),
            ),
          ),
          Center(
            child: Text(
              '\n\n\n\nAgradecimientos a:\n🔸Ariane Orué\n🔸Promocion de Marketing ETyC 2019',
              style: TextStyle(
                fontSize: 12.0,
                color: CupertinoColors.activeBlue,
                decorationColor: CupertinoColors.white,
                fontStyle: FontStyle.italic,
                fontFamily: 'SF-Pro-Text-Bold',
              ),
            ),
          )
        ],
      ),
    ),
    navigationBar: CupertinoNavigationBar(
      middle: Text(title),
    ),
  );
}

Widget _buildHomePage(BuildContext context) {
  return CupertinoPageScaffold(
    child: Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          GestureDetector(
            child: ImageCarousel(),
            onDoubleTap: () {
              print('double tap detected');
              _infoImagen(
                context,
                title[currentPage.round()],
                Text('Ko’ãga aikuaama'),
              );
            },
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CupertinoButton(
                color: CupertinoColors.activeBlue,
                child: Text(
                  'Aikuaaseve',
                  style: TextStyle(
                      fontSize: 16.5,
                      fontStyle: FontStyle.italic,
                      color: CupertinoColors.white,
                      decorationColor: CupertinoColors.white),
                ),
                onPressed: () {
                  _infoImagen(
                    context,
                    title[currentPage.round()],
                    Text('Ko’ãga aikuaama'),
                  );
                },
              ),
            ],
          ),
          Center(
            child: Padding(
              padding: EdgeInsets.all(18.0),
              child: GestureDetector(
                onTap: () {
                  print('double tap detected');
                  _infoImagen(
                    context,
                    title[currentPage.round()],
                    Text('Ko\'anga aikuaama'),
                  );
                },
                child: CupertinoButton(
                  color: CupertinoColors.activeGreen,
                  child: Text(
                    'Ambohasase',
                    style: TextStyle(
                        fontSize: 15.0,
                        fontStyle: FontStyle.italic,
                        color: CupertinoColors.white,
                        decorationColor: CupertinoColors.white),
                  ),
                  onPressed: () {
                    _shareMultimedia();
                  },
                ),
              ),
            ),
          )
        ],
      ),
    ),
    navigationBar: CupertinoNavigationBar(
      middle: Column(
        children: <Widget>[
          Center(
            child: Text(
              'Ñe\'enga Teller',
              style: TextStyle(
                fontSize: 30.0,
              ),
            ),
          )
        ],
      ),
    ),
  );
}

Widget _buildSupportPage() {
  return CustomScrollView(
    slivers: const <Widget>[
      CupertinoSliverNavigationBar(
        largeTitle: Text('Ore'),
      ),
    ],
  );
}

void _opcionCompatir(
    BuildContext context, Text question, Text opt1, Text opt2) {
  showCupertinoDialog(
      context: context,
      builder: (BuildContext context) => CupertinoAlertDialog(
            title: question,
            actions: [
              CupertinoDialogAction(
                child: opt1,
                isDestructiveAction: true,
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              CupertinoDialogAction(
                child: opt2,
                isDefaultAction: true,
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          ));
}

void _infoImagen(BuildContext context, String question, Text opt1) {
  var string = question.split(';');

  showCupertinoDialog(
    context: context,
    builder: (BuildContext context) => CupertinoAlertDialog(
      title: Text(string[0]),
      content: Text(string[1]),
      actions: [
        CupertinoDialogAction(
          child: opt1,
          isDestructiveAction: true,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ],
    ),
  );
}

void _shareMultimedia() async {
  var cadena = title[currentPage.round()];
  var split = cadena.split(';');
  var titulo = split[0];
  var contenido = split[1];

  final ByteData bytes = await rootBundle.load(images[currentPage.round()]);
  await Share.file(
      'Ñembohasa', 'image.png', bytes.buffer.asUint8List(), 'image/jpg',
      text: '$titulo\n\n$contenido');
}

class ZoomClipAssetImage extends StatelessWidget {
  const ZoomClipAssetImage(
      {@required this.zoom,
      this.height,
      this.width,
      @required this.imageAsset});

  final double zoom;
  final double height;
  final double width;
  final String imageAsset;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      alignment: Alignment.center,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: OverflowBox(
          maxHeight: height * zoom,
          maxWidth: width * zoom,
          child: Image.asset(
            imageAsset,
            fit: BoxFit.fill,
          ),
        ),
      ),
    );
  }
}
