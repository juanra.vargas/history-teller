List<String> images = [
  "assets/image_12.jpg",
  "assets/image_11.jpg",
  "assets/image_10.jpg",
  "assets/image_09.jpg",
  "assets/image_08.jpg",
  // "assets/image_07.jpg",
  // "assets/image_06.jpg",
  "assets/image_05.jpg",
  // "assets/image_04.jpg",
  // "assets/image_03.jpg",
  "assets/image_02.jpg",
  "assets/image_01.jpg",
];

List<String> title = [
  "Lunero;🔸Ésta palabra surge de la combinación de Lunes + Iro (en guaraní) y significa LUNES AMARGO🔸",
  "Ja'umina;🔸Expresión bastante conocida y celebrada en grupos de amigos, significa VAMOS A TOMAR y se usa para demostrar ansias de ingerir terere o, generalmente, bebidas alcohólicas🍺",
  "Jahamina mombyry;🔸Es una frase usada generalmente como invitación a viajar o pasear, y significa VAMOS LEJOS 🎒🛣🏕",
  "Toiko la oikoa;🔸Frase que significa QUE PASE LO QUE PASE 🍀",
  "Hendy kavaju resa;🔸Se traduce como BRILLAN LOS OJOS DEL CABALLO 💥👁🐴.\n\nEn el guaraní popular, esta frase se usa para expresar algún malestar o dificultad, generalmente económica 💸",
  // "Index 6",
  // "Index 5",
  "Che kuerai;🔶Frase utilizada para demostrar frustración y significa ESTOY HARTA/O 😾🤬",
  // "Index 3",
  // "Index 2",
  "Japuka mba\'e;🔸Frase que se utiliza normalmente para encarar alguna situación de manera positiva y significa VAMOS A REÍRNOS O QUE 🤪",
  "Ahata aju;🔸La traducción literal de la frase es ME VOY Y REGRESO y se usa comúnmente como despedida, entendiéndose que no hay intención de volver🚶🏻‍♂",
];
